//
//  ConfigViewController.m
//  Fusion Client iOS SDK App
//
//  Created by Thrupoint on 02/04/2013.
//  Copyright (c) 2013 Thrupoint. All rights reserved.
//

#import "ConfigViewController.h"

@interface ConfigViewController ()

@end

@implementation ConfigViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.password.secureTextEntry = true;
	self.started = false;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void) toggleStart:(UIButton *)sender
{
    if (!self.started) {
        self.started = true;
        [self.startStopButton setTitle:@"Stop" forState:UIControlStateNormal];
        self.view.backgroundColor = [UIColor colorWithRed:0.5 green:0.8 blue:0.5 alpha:1.0];
    }
    else {
        self.started = false;
        [self.startStopButton setTitle:@"Start" forState:UIControlStateNormal];
        self.view.backgroundColor = [UIColor whiteColor];
    }
}

-(IBAction)userDoneEnteringText:(id)sender
{
    [sender resignFirstResponder];
}

@end