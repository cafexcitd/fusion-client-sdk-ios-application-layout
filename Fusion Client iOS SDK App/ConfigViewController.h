//
//  ConfigViewController.h
//  Fusion Client iOS SDK App
//
//  Created by Thrupoint on 02/04/2013.
//  Copyright (c) 2013 Thrupoint. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ACBClientSDK/ACBUC.h>

@interface ConfigViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *username;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UIButton *startStopButton;

@property (atomic) bool started;

-(IBAction)toggleStart:(UIButton*)sender;

@end
